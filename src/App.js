import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from '../src/views/Home'
import ListSearch from '../src/views/ListSearch'

function App () {
  return (
    <Router>
      <Switch>
        <Route path='/list'>
          <ListSearch />
        </Route>
        <Route path='/'>
          <Home />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
