import React from 'react'
import Skeleton from 'react-loading-skeleton'

const SkeletonCard = (props) => {
  return (
    <div className='container-card'>
      <div>
        <Skeleton height={150} width='100%' />
      </div>
      <div className='content-card'>
        <div className='title-video'>
          <Skeleton width='50%' />
        </div>
        <div className='footer-card'>
          <div className='channel-name'>
            <Skeleton width='20%' />

          </div>
          <div className='published-date'>
            <Skeleton width='10%' />

          </div>
        </div>
      </div>

    </div>
  )
}

export default SkeletonCard
