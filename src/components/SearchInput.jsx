import React from 'react'
import './SearchInput.css'
import { BiSearch } from 'react-icons/bi'

const SearchInput = (props) => {
  return (
    <div className={`wrap-search-input ${props.type == 'large' ? 'wrap-search-large' : 'wrap-search-small'}`}>
      <input
        className={`search-input ${props.type == 'large' ? 'search-input-large' : 'search-input-small'}`}
        type='text'
        name='search'
        onChange={props.onChange}
        value={props.value}
        onKeyPress={props.onKeyPress}
      />
      <BiSearch className='icon-search' />
    </div>
  )
}

export default SearchInput
