import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Button.css'

const Button = (props) => {
  return (
    <div className='wrap-button'>
      <Link to='/list?q=hallo+word'>
        <button className='button'>
          Search
        </button>
      </Link>
    </div>
  )
}

export default Button
