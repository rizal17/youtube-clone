import React from 'react'
import './CardVideo.css'
import moment from 'moment'

const CardVideo = (props) => {
  const { channelTitle, thumbnails, title, publishTime } = props.data.snippet

  return (
    <div className='container-card'>
      <div>
        <img className='thumb' src={thumbnails.medium.url} alt='' />
      </div>
      <div className='content-card'>
        <div className='title-video'>
          {title.length > 50 ? title.substr(0, 50) + '...' : title}
        </div>
        <div className='footer-card'>
          <div className='channel-name'>
            {channelTitle}
          </div>
          <div className='published-date'>
            {moment(publishTime).fromNow()}
          </div>
        </div>
      </div>

    </div>
  )
}

export default CardVideo
