import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Home.css'
import SearchInput from '../components/SearchInput'
import Button from '../components/Button'
import { withRouter } from "react-router-dom";

class Home extends Component {

    constructor(props){
        super(props);
        this.state={
            value:''
        }
    }

    handleKeyPress=(e)=>{
        if(e.charCode==13){
            this.props.history.push(`/list?q=${this.state.value}`)
        }
    }

    handleChange=(e)=>{
        this.setState({value:e.target.value})
    }
    
    render () {
        return (
        <div className='container-page'>
            <SearchInput type="large" 
                        onKeyPress={this.handleKeyPress}
                        onChange={this.handleChange}
                        value={this.state.value}/>
            <Button to="/list"/>
        </div>
        )
    }
}

export default withRouter(Home)
