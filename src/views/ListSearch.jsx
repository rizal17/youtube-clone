import React, { Component } from 'react'
import SearchInput from '../components/SearchInput'
import CardVideo from '../components/CardVideo'
import SkeletonCard from '../components/SkeletonCard'
import './ListSearch.css'
import { withRouter } from "react-router-dom";
import axios from 'axios'

class ListSearch extends Component {
    constructor(props){
        super(props);
        this.state={
            value:'',
            data:{},
            pageToken:'',
            isLoading:false
            
        }
    }


    handleChange=(e)=>{
        this.setState({value:e.target.value})
    }

    handleKeyPress=(e)=>{
        if(e.charCode==13){
            this.props.history.push(`/list?q=${this.state.value}`)
            this.fetchApiData()

        }
    }

    fetchApiData=(value)=>{
        this.setState({isLoading:true})

        const params = {
            key:process.env.REACT_APP_API_KEY,
            q:this.state.value.length>0?this.state.value:value,
            part:'snippet',
            pageToken:this.state.pageToken,
            maxResults:10,
            type:'video'
        }
        axios.defaults.baseURL = process.env.REACT_APP_API_URL;

        axios.get('/youtube/v3/search',{params})
                .then((res)=>{
                    this.setState({isLoading:false,data:res.data})


                })
                .catch((err)=>{
                    console.log(err,'err')
                })
    }

  componentDidMount(){
      const qs = new URLSearchParams(this.props.location.search)
      this.setState({value:qs.get('q')})
      this.fetchApiData(qs.get('q'))

  }

    
  render () {
    return (
      <div className='container-list'>
        <div className='header-list'>
          <SearchInput type='small'
                        value={this.state.value}
                        onChange={this.handleChange}
                        onKeyPress={this.handleKeyPress}/>
        </div>
        <div className='container-content'>
            {this.state.isLoading && Array(10).fill().map((item,i)=>(
                <SkeletonCard item={item}/>
            ))}
            {!this.state.isLoading && this.state.data.items?(
                
                this.state.data.items.map((item,i)=>(
                    <CardVideo data={item} key={i}/>
            ))):(<div></div>)}
        </div>
      </div>
    )
  }
}

export default withRouter(ListSearch)
